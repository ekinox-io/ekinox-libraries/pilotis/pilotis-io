from pathlib import Path

import pytest
from hamcrest import assert_that, contains_inanyorder, empty, equal_to

from pilotis_io.exceptions import PilotisIoError
from pilotis_io.io import IoAPI
from pilotis_io.local import LocalIoApi


@pytest.fixture
def local_api(tmp_path: Path) -> IoAPI:
    return LocalIoApi(project_dir=str(tmp_path))


def test_get_path_uri_with_file(local_api: IoAPI):
    # Given a file descriptor relative to project root
    relative_file = Path(".")

    # When
    uri = local_api.get_path_uri(relative_file)

    # Then
    absolut_file = (local_api.project_root_path / relative_file).absolute()
    assert_that(uri, equal_to(str(absolut_file)))


def test_get_path_url_with_none(local_api: IoAPI):
    # When / Then
    with pytest.raises(PilotisIoError):
        local_api.get_path_uri(None)


def test_file_exists_when_file_exists(local_api: IoAPI):
    # Given a file placed in the API's directory
    some_file = local_api.project_root_path / "test.txt"
    some_file.touch()

    # When testing file existence
    result: bool = local_api.file_exists(Path("test.txt"))

    # Then it should detect it
    assert_that(result)


def test_file_exists_when_file_is_a_directory(local_api: IoAPI):
    # Given a non-empty directory in the API's directory
    some_dir = local_api.project_root_path / "directory"
    some_dir.mkdir()
    some_file = some_dir / "test.txt"
    some_file.touch()

    # When testing file existence
    result: bool = local_api.file_exists(Path("directory"))

    # Then it should detect it
    assert_that(result)


def test_file_exists_when_file_does_not_exists(local_api: IoAPI):
    # Given that no file is placed manually under thee API's directory
    # When testing some non existing file existence
    result: bool = local_api.file_exists(Path("test.txt"))

    # Then it should not detect it
    assert_that(not result)


def test_file_exists_when_file_is_none(local_api: IoAPI):
    # When
    result: bool = local_api.file_exists(None)

    # Then
    assert_that(not result)


def test_store_file_should_move_file(local_api: IoAPI):
    # Given an existing file
    file_relative_path = Path("test.txt")
    file_full_path = local_api.project_root_path / file_relative_path
    file_full_path.touch()

    # And a destination path
    destination_relative_path = Path("destination.txt")
    destination_full_path = local_api.project_root_path / destination_relative_path

    # When
    local_api.store_file(file_full_path, destination_relative_path)

    # Then
    assert_that(not file_full_path.exists())
    assert_that(destination_full_path.exists())


def test_store_file_should_create_parent_directory_if_not_exists(local_api: IoAPI):
    # Given an existing file
    file_relative_path = Path("test.txt")
    file_full_path = local_api.project_root_path / file_relative_path
    file_full_path.touch()

    # And a destination path in a non existing directory
    destination_relative_path = Path("target_directoory") / "destination.txt"
    destination_full_path = local_api.project_root_path / destination_relative_path

    # When
    local_api.store_file(file_full_path, destination_relative_path)

    # Then
    assert_that(not file_full_path.exists())
    assert_that(destination_full_path.exists())


def test_store_file_should_work_with_directories(local_api: IoAPI):
    # Given an existing file in a directory
    source_directory_relative_path = Path("source_dir")
    source_directory_full_path = (
        local_api.project_root_path / source_directory_relative_path
    )
    source_directory_full_path.mkdir()
    file_relative_path = Path("test.txt")
    file_full_path = source_directory_full_path / file_relative_path
    file_full_path.touch()

    # And a destination path
    destination_relative_path = Path("destination_directory")
    destination_full_path = local_api.project_root_path / destination_relative_path

    # When
    local_api.store_file(source_directory_full_path, destination_relative_path)

    # Then
    assert_that(not source_directory_full_path.exists())
    assert_that(destination_full_path.exists())
    assert_that(destination_full_path.is_dir())


def test_store_file_with_no_source_path(local_api: IoAPI):
    # Given a destination path
    destination_relative_path = Path("destination.txt")

    # When / Then
    with pytest.raises(PilotisIoError):
        local_api.store_file(None, destination_relative_path)


def test_store_file_with_no_destination_path(local_api: IoAPI):
    # Given an existing file
    file_relative_path = Path("test.txt")
    file_full_path = local_api.project_root_path / file_relative_path
    file_full_path.touch()

    # When / Then
    with pytest.raises(PilotisIoError):
        local_api.store_file(file_full_path, None)


def test_copy_or_symlink_to_local(local_api: IoAPI):
    # Given symlink definition file
    relative_symlink_path = Path("symlink.txt")

    # And a target file
    relative_target_file = Path("target.txt")
    full_target_path = local_api.project_root_path / relative_target_file
    full_target_path.touch()

    # When
    full_symlink_path = local_api.copy_or_symlink_to_local(
        relative_symlink_path, relative_target_file
    )

    # Then the target file is a symlink
    assert_that(full_symlink_path.exists())
    assert_that(full_symlink_path.is_symlink())


def test_copy_or_symlink_to_local_with_no_symlink_path(local_api: IoAPI):
    # Given a target file
    relative_target_file = Path("target.txt")
    full_target_path = local_api.project_root_path / relative_target_file
    full_target_path.touch()

    # When
    with pytest.raises(PilotisIoError):
        local_api.copy_or_symlink_to_local(None, relative_target_file)


def test_copy_or_symlink_to_local_with_no_target_definition(local_api: IoAPI):
    # Given symlink definition file
    relative_symlink_path = Path("symlink.txt")

    # When
    with pytest.raises(PilotisIoError):
        local_api.copy_or_symlink_to_local(relative_symlink_path, None)


def test_copy_or_symlink_to_local_with_non_existing_target(local_api: IoAPI):
    # Given symlink definition file
    relative_symlink_path = Path("symlink.txt")

    # And a non existing target file
    relative_target_file = Path("target.txt")

    # When
    with pytest.raises(PilotisIoError):
        local_api.copy_or_symlink_to_local(relative_symlink_path, relative_target_file)


def test_copy_or_symlink_to_local_with_already_existing_symlink(local_api: IoAPI):
    # Given symlink definition file
    relative_symlink_path = Path("symlink.txt")
    full_symlink_path = local_api.project_root_path / relative_symlink_path
    full_symlink_path.touch()

    # And a target file
    relative_target_file = Path("target.txt")
    full_target_path = local_api.project_root_path / relative_target_file
    full_target_path.touch()

    # When
    with pytest.raises(PilotisIoError):
        local_api.copy_or_symlink_to_local(relative_symlink_path, relative_target_file)


def test_list_files_in_dir(local_api: IoAPI):
    # Given
    relative_dir_path = Path("directory")
    full_dir_path = local_api.project_root_path / relative_dir_path
    full_dir_path.mkdir()
    relative_file_1_path = relative_dir_path / "file1.txt"
    full_file_1_path = local_api.project_root_path / relative_file_1_path
    full_file_1_path.touch()
    relative_file_2_path = relative_dir_path / "file2.txt"
    full_file_2_path = local_api.project_root_path / relative_file_2_path
    full_file_2_path.touch()

    # When
    files = local_api.list_files_in_dir(relative_dir_path)

    # Then
    assert_that(files, contains_inanyorder(relative_file_1_path, relative_file_2_path))


def test_list_files_in_dir_whith_empty_directory(local_api: IoAPI):
    # Given
    relative_dir_path = Path("directory")
    full_dir_path = local_api.project_root_path / relative_dir_path
    full_dir_path.mkdir()

    # When
    files = local_api.list_files_in_dir(relative_dir_path)

    # Then
    assert_that(files, empty())


def test_list_files_in_dir_with_no_directory(local_api: IoAPI):
    # When / Then
    with pytest.raises(PilotisIoError):
        local_api.list_files_in_dir(None)


def test_list_files_in_dir_is_not_recursive(local_api: IoAPI):
    # Given
    relative_dir_path = Path("directory")
    full_dir_path = local_api.project_root_path / relative_dir_path
    full_dir_path.mkdir()
    relative_file_1_path = relative_dir_path / "file1.txt"
    full_file_1_path = local_api.project_root_path / relative_file_1_path
    full_file_1_path.touch()
    relative_sub_dir_path = relative_dir_path / "sub_directory"
    full_sub_dir_path = local_api.project_root_path / relative_sub_dir_path
    full_sub_dir_path.mkdir()
    relative_file_2_path = relative_sub_dir_path / "file2.txt"
    full_file_2_path = local_api.project_root_path / relative_file_2_path
    full_file_2_path.touch()

    # When
    files = local_api.list_files_in_dir(relative_dir_path)

    # Then
    assert_that(files, contains_inanyorder(relative_file_1_path))


def test_mk_dir_creates_directory_into_the_working_directory(local_api: IoAPI):
    # Given
    directory_path = Path("directory")

    # When
    absolute_path = local_api.mk_dir(directory_path)

    # Then
    assert_that(absolute_path.exists())
    assert_that(absolute_path.is_dir())
    assert_that(absolute_path.name, equal_to(directory_path.name))


def test_mk_dir_creates_intermediate_directories(local_api: IoAPI):
    # Given
    directory_path = Path("directory")
    sub_directory_path = directory_path / "sub_directory"

    # When
    absolute_path = local_api.mk_dir(sub_directory_path)

    # Then
    assert_that(absolute_path.exists())
    assert_that(absolute_path.is_dir())
    assert_that(absolute_path.name, equal_to(sub_directory_path.name))
    assert_that(absolute_path.parent.name, equal_to(directory_path.name))


def test_mk_dir_just_return_the_directory_if_already_exists(local_api: IoAPI):
    # Given
    directory_path = Path("directory")
    absolute_path = local_api.project_root_path / directory_path
    absolute_path.mkdir()

    # When
    returned_absolute_path = local_api.mk_dir(directory_path)

    # Then
    assert_that(returned_absolute_path.exists())
    assert_that(returned_absolute_path.is_dir())
    assert_that(returned_absolute_path.name, equal_to(directory_path.name))


def test_mk_dir_when_no_directory(local_api: IoAPI):
    # When / Then
    with pytest.raises(PilotisIoError):
        local_api.mk_dir(None)
