# file modified from https://gist.github.com/lumengxi/0ae4645124cd4066f676
.PHONY: *

#################################################################
# Shared variables
#################################################################

PACKAGE_DIR=pilotis_io

define BROWSER_PYSCRIPT
import os, webbrowser, sys
try:
	from urllib import pathname2url
except:
	from urllib.request import pathname2url

webbrowser.open("file://" + pathname2url(os.path.abspath(sys.argv[1])))
endef
export BROWSER_PYSCRIPT
BROWSER := python -c "$$BROWSER_PYSCRIPT"

#################################################################
# Shared functions
#################################################################

# Check that given variables are set and all have non-empty values,
# die with an error otherwise.
#
# Params:
#   1. Variable name(s) to test.
#   2. (optional) Error message to print.
# Details at https://stackoverflow.com/a/10858332/4374048
check_defined = \
    $(strip $(foreach 1,$1, \
        $(call __check_defined,$1,$(strip $(value 2)))))
__check_defined = \
    $(if $(value $1),, \
      $(error Undefined $1$(if $2, ($2)): please pass as argument (see help target)))


#################################################################
# help
#################################################################

help:
	@echo "SETUP TARGETS:"
	@echo "\tsetup-dev-env-minimal - setup minimal dev environment for all make targets to work"
	@echo "\tsetup-dev-env-full - setup full dev environment to allow IDE completion"
	@echo "\tsetup-dev-host - setup host dev requirements"
	@echo "\tsetup-pre-commit-hooks - setup pre-commit hooks"
	@echo ""
	@echo "MAIN TARGETS:"
	@echo "\tclean - remove all build, test, coverage and Python artifacts"
	@echo "\ttest - run unit tests"
	@echo "\tlint - check style with flake8"
	@echo "\tformat - enforce correct format with isort (after a seed-isort-config) and black"
	@echo "\tformat-check - check format for compliance with black and isort"
	@echo "\ttype - check Python typing"
	@echo "\trepl - run the repl tool (bpython in our case)"
	@echo ""
	@echo ""
	@echo "GIT TARGETS:"
	@echo "\tprune-branches - prune obsolete local tracking branches and local branches"
	@echo "\tprune-branches-force|pbf - as above but force delete local branches"
	@echo "\tpost-PR-merge-sync|pms - switch to master, pull and run pbf target"


#################################################################
# setting up dev env
#################################################################

setup-dev-env-minimal: clean
	poetry install --no-root -E format

setup-dev-env-full: clean
	poetry install --no-root -E test -E type -E format -E lint -E repl

setup-dev-host:
	./scripts/install_pyenv.sh
	./scripts/install_poetry.sh
	@echo "Host setup correctly. Restart your shell or source your shell config file to be up and running :)"


#################################################################
# setting dependencies for automation (tox, cicd)
#################################################################

install-test-dependencies :
	poetry install --no-dev --no-root -E test

install-format-dependencies:
	poetry install --no-dev --no-root -E format

install-lint-dependencies:
	poetry install --no-dev --no-root -E lint

install-type-dependencies:
	poetry install --no-dev --no-root -E type


#################################################################
# setting up ci-cd env
#################################################################

setup-cicd-python3:
	update-alternatives --install /usr/bin/python python /usr/bin/python3 1
	curl -sSL  https://bootstrap.pypa.io/get-pip.py | python

setup-cicd-poetry:
	curl -sSL https://raw.githubusercontent.com/sdispater/poetry/master/get-poetry.py | python
	. $(HOME)/.poetry/env && poetry config virtualenvs.create false
	echo "WARNING: you still need to source $$HOME/.poetry/env to access poetry's executable"

setup-cicd-test-stage: setup-cicd-poetry

setup-cicd-release-stage: setup-cicd-python3 setup-cicd-poetry setup-release-tools


#################################################################
# cleaning
#################################################################

clean: clean-build clean-pyc clean-test

clean-build:
	rm -fr build/
	rm -fr dist/
	rm -fr .eggs/
	find . -name '*.egg-info' -exec rm -fr {} +
	find . -name '*.egg' -exec rm -f {} +

clean-pyc:
	find . -name '*.pyc' -exec rm -f {} +
	find . -name '*.pyo' -exec rm -f {} +
	find . -name '*~' -exec rm -f {} +
	find . -name '__pycache__' -exec rm -fr {} +

clean-test:
	rm -fr .tox/
	rm -f .coverage
	rm -fr htmlcov/

clean-venv:
	# poetry env remove might not to work if `virtualenvs.in-project = true`
	# (see https://github.com/python-poetry/poetry/issues/2124)
	# so if not, remove whole `.venv` directory using https://unix.stackexchange.com/questions/153763
	poetry env remove $$(poetry env info -p)/bin/python && ([ $$? -eq 0 ]) || rm -rf $$(poetry env info -p)

#################################################################
# repl
#################################################################

repl:
	poetry run bpython

#################################################################
# linting
#################################################################

lint:
	poetry run flake8 $(PACKAGE_DIR) tests

#################################################################
# formating
#################################################################

seed-isort:
	poetry run seed-isort-config

isort:
	poetry run isort -rc $(PACKAGE_DIR) tests -vb

black:
	poetry run black $(PACKAGE_DIR) tests

format: seed-isort isort black

format-check:
	poetry run isort -c -rc $(PACKAGE_DIR) tests -vb
	poetry run black --check $(PACKAGE_DIR) tests


#################################################################
# typing
#################################################################

type:
	poetry run mypy -p $(PACKAGE_DIR) -p tests

#################################################################
# unit testing
#################################################################

test:
	poetry run pytest --cov=$(PACKAGE_DIR) --cov-report=html --cov-report=term tests


#################################################################
# installing developed package/library
#################################################################

install: clean
	poetry install --no-dev

#################################################################
# git targets
#################################################################

prune-branches:
	git remote prune origin
	git branch -vv | grep ': gone]'|  grep -v "\*" | awk '{ print $$1; }' | xargs git branch -d


prune-branches-force:
	git remote prune origin
	git branch -vv | grep ': gone]'|  grep -v "\*" | awk '{ print $$1; }' | xargs git branch -D

pbf: prune-branches-force

post-PR-merge-sync-step-1:
	git switch master
	git pull

post-PR-merge-sync: post-PR-merge-sync-step-1 prune-branches-force

pms: post-PR-merge-sync
